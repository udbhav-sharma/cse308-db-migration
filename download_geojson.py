import argparse
import json
import mysql.connector

from os.path import join

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train Model')

    parser.add_argument('--user', type=str, required=True, help="Connecting user")
    parser.add_argument('--password', type=str, required=True, help="Connecting user's password")
    parser.add_argument('--host', type=str, required=True, help="MySQL server host")
    parser.add_argument('--config', type=str, required=True, help="Database query configuration")

    args = parser.parse_args()

    print("Loading Config: " + args.config)
    with open(args.config) as f: config = json.load(f)
    print("Config loaded")

    print("Connecting to database: [{}] with user: [{}] from: [{}]".format(config["database"], args.user, args.host))
    cnx = mysql.connector.connect(
        user=args.user, password=args.password, host=args.host, database=config["database"])
    print("Connection established")

    output_file = join('data', config['output_geojson'])

    print("Downloading geojson to {}".format(output_file))
    cursor = cnx.cursor()
    cursor.execute(config['query'])
    with open(output_file, 'w') as o:
        for (state, precinct, geojson) in cursor:
            o.write("{} | {} | {}\n".format(state, precinct, geojson))
    print("Downloaded geojson")

    cnx.close()

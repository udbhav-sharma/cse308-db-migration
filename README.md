## Requirements

Python - 3

## Setup

1. Download/Clone repository
2. Then run `pip install -r requirements.txt`

## Files

[config](config) contains configuration files for different teams.

[download_geojson.py](download_geojson.py) downloads geojson data based on the config passed.

[Teams-Data-Analysis.txt](Teams-Data-Analysis.txt) Contains db analysis of various teams.

## Running Code

For downloading steers data

    python download_geojson.py --host <HOST> --user <USER> --password <PASSWORD> --config config/steers.json

Data is downloaded in [data](data) directory with name as given in the config.

Data format is `<STATE> | <PRECINCT> | <GEO_JSON>`
